# language: no
Egenskap: Bruker1

 Som kunde av nettkiosken
 Ønsker jeg å legge varer i handlekurven
 Slik at jeg kan få godis i posten

Scenario:
    Gitt at jeg har åpnet nettkiosken
    Når jeg legger inn varer og kvanta
    Så skal handlekurven inneholde det jeg har lagt inn
    Og den skal ha riktig totalpris

Scenario:
    Gitt at jeg har åpnet nettkiosken
    Og lagt inn varer og kvanta
    Når jeg sletter varer
    Så skal ikke handlekurven inneholde det jeg har slettet
    
Scenario:
    Gitt at jeg har åpnet nettkiosken
    Og lagt inn varer og kvanta
    Når jeg oppdaterer kvanta for en vare
    Så skal handlekurven inneholde riktig kvanta for varen
