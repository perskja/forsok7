import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

// SCENARIO 1:
// Gitt at jeg har lagt inn varer i handlekurven
// Og trykket på Gå til betaling
// Når jeg legger inn navn, adresse, postnummer, poststed og kortnummer
// Og trykker på Fullfør kjøp
// Så skal jeg få beskjed om at kjøpet er registrert

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();
    
    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
   
     cy.get('#fullName').type('PER');
     cy.get('#address').type('PER');
     cy.get('#postCode').type('1234');
     cy.get('#city').type('PER');
     cy.get('#creditCardNo').type('1231456321456333');
  
});

And(/^trykker på Fullfør kjøp$/, function () {
    cy.get('.formField input[type="submit"][value="Fullfør handel"]').click();
    
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').should('contain', 'Din ordre er registrert.');
    cy.url().should('include', '/receipt.html');
      });
      

// SCENARIO 2:
// Gitt at jeg har lagt inn varer i handlekurven
// Og trykket på Gå til betaling
// Når jeg legger inn ugyldige verdier i feltene
// Så skal jeg få feilmeldinger for disse

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();
    
    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').clear();   
    cy.get('#address').clear();  
    cy.get('#postCode').clear();   
    cy.get('#city').clear();  
    cy.get('#creditCardNo').clear().type('123');   
    });
    

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    cy.get('.formField input[type="submit"][value="Fullfør handel"]').click(); // Tvinger fram vurdering, funksjonellt, men kanskje en bedre måte å gjøre det på?

    cy.get('#fullNameError').should('exist').and('contain', 'Feltet må ha en verdi');
    cy.get('#addressError').should('exist').and('contain', 'Feltet må ha en verdi');
    cy.get('#postCodeError').should('exist').and('contain', 'Feltet må ha en verdi');
    cy.get('#cityError').should('exist').and('contain', 'Feltet må ha en verdi');
    cy.get('#creditCardNoError').should('exist').and('contain', 'Kredittkortnummeret må bestå av 16 siffer');
        });
        
