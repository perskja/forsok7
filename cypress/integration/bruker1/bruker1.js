import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

// SCENARIO 1:
// Gitt at jeg har åpnet nettkiosken
// Når jeg legger inn varer og kvanta
// Så skal handlekurven inneholde det jeg har lagt inn
// Og den skal ha riktig totalpris

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
   
    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    cy.get('#product').should('contain', 'Smørbukk');
    cy.get('#product').should('contain', 'Stratos');
    cy.get('#product').should('contain', 'Hobby');

});

And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '25'); //Om alt er lagt sammen riktig skal prisen være 25
});

// SCENARIO 2:
//Gitt at jeg har åpnet nettkiosken
 //   Og lagt inn varer og kvanta
 //   Når jeg sletter varer
 //   Så skal ikke handlekurven inneholde det jeg har slettet

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, function () {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

});

When(/^jeg sletter varer$/, () => {
    
    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#deleteItem').click();

});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
        cy.get('#product').should('contain', 'Hubba bubba');

      });

// SCENARIO 3:
// Gitt at jeg har åpnet nettkiosken
// Og lagt inn varer og kvanta
// Når jeg oppdaterer kvanta for en vare
// Så skal handlekurven inneholde riktig kvanta for varen

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();


});

When(/^jeg oppdaterer kvanta for en vare$/, function () {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('3');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
    cy.get('#cart').should('contain', 'Hubba bubba'); // THIS WORKS, SO 
    cy.get('#cart').should('contain', 3); //WHY DOES THIS NOT WORK? 
  });
  


